// function avilistopened
//    Show all opened video files. (experimental)
//    
//    Syntax
//    I=avilistopened()
//    
//    Parameters
//    I : A vector, the opened video file/camera indices.
//    
//    Description
//    avilistopenedlist all opened files and cameras.
//    
//    Video support for IPCV is only available when IPCV is compiled with OpenCV which support video I/O.
//    
//    Examples
//    n = aviopen(fullpath(getIPCVpath() + "/images/video.avi"));
//    im = avireadframe(n); //get a frame
//    imshow(im);
//    
//    avilistopened()
//    aviclose(n);
//     
//    See also
//    aviinfo
//    aviopen
//    camopen
//    avifile
//    addframe
//    aviclose
//    avicloseall
//    avireadframe
//    
//    Authors
//    Shiqi Yu 
//    Tan Chin Luh
//    
//        
//
