// function avireadframe
//    Grabs and returns a frame from a opened video file or camera (experimental)
//    
//    Syntax
//    im = avireadframe(n)
//    
//    Parameters
//    n : The opened video file/camera index.
//    im : The returned frame/image. If no frame, return 0.
//    
//    Description
//    avireadframe grabs and returns a frame from an opened video file or camera. It can only get the current frame and move to the pointer to the next frame.
//    
//    Video support for IPCV is only available when IPCV is compiled with OpenCV which support video I/O.
//    
//    Examples
//    n = aviopen(fullpath(getIPCVpath() + "/images/video.avi"));
//    im = avireadframe(n); //get a frame
//    imshow(im);
//    
//    avilistopened()
//    aviclose(n);
//     
//    See also
//    aviinfo
//    aviopen
//    camopen
//    avifile
//    addframe
//    aviclose
//    avicloseall
//    avilistopened
//    
//    Authors
//    Shiqi Yu
//    Tan Chin Luh

