/***********************************************************************
 * IPCV - Scilab Image Processing and Computer Vision toolbox
 * Copyright (C) 2017  Tan Chin Luh
 ***********************************************************************/

#include "common.h"

/**********************************************************************
* This function is using the OpenCV highgui function for plotting image
* imdisplay(imin);
**********************************************************************/
int sci_int_imwrite(char * fname,void* pvApiCtx)
{
	SciErr sciErr;

	//// Cleaning up
	//cvReleaseImage( &pSrcImg );
	//cvReleaseImage( &pDstImg );
	CheckInputArgument(pvApiCtx, 2, 2);
	CheckOutputArgument(pvApiCtx, 0, 1);

	/////////////////
	// First Input //
	/////////////////
	Mat pSrcImg;
	int* piAddr = NULL;
	char* pstData = NULL;
	int iRet    = 0;
	GetImage(1,pSrcImg,pvApiCtx);
	sciprint(".");
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
	iRet = getAllocatedSingleString(pvApiCtx, piAddr, &pstData);
		if(iRet)
		{
			freeAllocatedSingleString(pstData);
			return iRet;
		}

	//printf
	/////////////////
	//Mat pDstImg = pSrcImg;
	//
	//dct( pSrcImg, pDstImg,1);
	//sciprint("dims : %s\n",pstData);
	//sciprint("dims : %i\n",pSrcImg.dims);
	//
	//SetImage(1,pDstImg);
	//namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    //imshow( "Display window", pSrcImg );                   // Show our image inside it.

    //waitKey(0);                           
	imwrite( pstData, pSrcImg );

	return 0;

	////////////

	//int mR2, nR2, lR2;
	//char *fn;
	//fn = "Original Image";
	//IplImage* pSrcImg = NULL;

	//CheckRhs(1, 2);
	//CheckLhs(1, 1);


	////load the input image
	//pSrcImg = Mat2IplImg(1);
	//
	//if (Rhs == 2)
	//{
	//	GetRhsVar(2, "c", &mR2, &nR2, &lR2);
	//	fn = cstk(lR2);
	//}

	//if(pSrcImg == NULL)
	//{
	//	Scierror(999, "%s: Internal error for getting the image data.\r\n", fname);
	//	return -1;
	//}

	// /// Create Windows
	//namedWindow(fn, 1);

	///// Show stuff
	//cvShowImage(fn,pSrcImg);
 //	waitKey(1);
	//cvReleaseImage(&pSrcImg);


	//return 0;
}
