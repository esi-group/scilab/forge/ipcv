/***********************************************************************
 * SIVP - Scilab Image and Video Processing toolbox
 * Copyright (C) 2005,2010  Shiqi Yu
 *
 * IPCV - Scilab Image Processing and Computer Vision toolbox
 * Copyright (C) 2017  Tan Chin Luh
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***********************************************************************/
/***********************************************************************
 *  Copyright (C) Trity Technologies - 2013 -
 * http://www.gnu.org/licenses/gpl-2.0.txt
 ***********************************************************************/

#include "common.h"

int sci_camread(char * fname,void* pvApiCtx)
{
  
  int mR1, nR1, lR1;
  int mR2, nR2, lR2;
  int mR3, nR3, lR3;
  int szd[] = {640,480};
  int *sz = &szd[0];
  int nFile;
  double *out = NULL;
  	int iRows			= 0;
	int iCols			= 0;
  IplImage * pImage;

  double tmp;

  //CheckRhs(1,2);
  //CheckLhs(0,1);
  CheckInputArgument(pvApiCtx, 1, 2);
  CheckOutputArgument(pvApiCtx, 0, 1);

 // GetRhsVar(1, "i", &mR1, &nR1, &lR1);
 // CheckDims(1, mR1, nR1, 1, 1);

 //if (Rhs == 2)
	//{
	//  GetRhsVar(2, "i", &mR3, &nR3, &lR3);
 //	   CheckDims(2, mR3, nR3, 1, 2);
	//   sz = istk(lR3);
	//}
  GetDouble(1,out,iRows,iCols,pvApiCtx);

  //nFile = *((int *)(istk(lR1)));
  nFile =  round(*out);

  nFile -= 1;

  if (!(nFile >= 0 && nFile < MAX_AVI_FILE_NUM))
    {
      Scierror(999, "%s: The argument should >=1 and <= %d.\r\n", fname, MAX_AVI_FILE_NUM);
      return -1;
    }


  if(! OpenedAviCap[nFile].video.cap)
    {
      Scierror(999, "%s: The %d'th file is not opened.\r\n Please use avilistopened command to show opened files.\r\n",
         fname, nFile+1);
      return -1;
    }

  //cvGrabFrame(OpenedAviCap[nFile].video.cap);
  //cvGrabFrame(OpenedAviCap[nFile].video.cap);
  //cvGrabFrame(OpenedAviCap[nFile].video.cap);
  //cvGrabFrame(OpenedAviCap[nFile].video.cap);

  pImage = cvQueryFrame(OpenedAviCap[nFile].video.cap);
  //pImage = cvRetrieveFrame(OpenedAviCap[nFile].video.cap,1);

  Mat dst = cvarrToMat(pImage);

  SetImage(1,dst,pvApiCtx);
	
	
	//pImage = cvQueryFrame(OpenedAviCap[nFile].video.cap);


  //if (! pImage)
  //{
  //    Create2DDoubleMat(Rhs+1, 0, 0, &tmp);
  //}
  //else if(! IplImg2Mat(pImage, Rhs+1))
  //  {
  //    Scierror(999, "%s: SIVP interal error.\r\n", fname);
  //    return -1;
  //  }

  //LhsVar(1) = Rhs+1;

  //pImage can not be released!!

  return 0;
	/////////
  
 /*if (Rhs == 2)
	{
	  GetRhsVar(2, "i", &mR3, &nR3, &lR3);
 	   CheckDims(2, mR3, nR3, 1, 2);
	   sz = istk(lR3);
	}*/

//  nFile = *((int *)(istk(lR1)));
//  nFile -= 1;
//
//  if (!(nFile >= 0 && nFile < MAX_AVI_FILE_NUM))
//    {
//      Scierror(999, "%s: The argument should >=1 and <= %d.\r\n", fname, MAX_AVI_FILE_NUM);
//      return -1;
//    }
//
//
//  if(! OpenedAviCap[nFile].video.cap)
//    {
//      Scierror(999, "%s: The %d'th file is not opened.\r\n Please use avilistopened command to show opened files.\r\n",
//         fname, nFile+1);
//      return -1;
//    }
//  //if(Rhs ==2 && nFrameIdx < 0)
//  //  {
//  //    Scierror(999, "%s: The frame index should >=1, but your input is %d.\r\n", fname, nFrameIdx+1);
//  //    return -1;
//  //  }
//
//
////  	cvSetCaptureProperty(OpenedAviCap[nFile].video.cap, CV_CAP_PROP_FRAME_WIDTH, *sz);
////	cvSetCaptureProperty(OpenedAviCap[nFile].video.cap, CV_CAP_PROP_FRAME_HEIGHT, *(sz+1));
//
//
//  pImage = cvQueryFrame(OpenedAviCap[nFile].video.cap);
//
//
//  //////////////// int* piAddr = NULL;
//                sciErr = getHypermatOfUnsignedInteger8(pvApiCtx, piAddr, &dims, &ndims, (unsigned char*)&data);
//                if(sciErr.iErr)
//                {
//                    printError(&sciErr, 0);
//                    return sciErr.iErr;
//                }
//
//  	sciErr = createHypermatOfUnsignedInteger8(pvApiCtx, nbInputArgument(pvApiCtx) + 1, dims, ndims, (const unsigned char*)pMatData);
//	if (sciErr.iErr)
//	{
//		printError(&sciErr, 0);
//		return sciErr.iErr;
//	}
//
//	AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
//	}
//
//  /////////////////////
//  //if (! pImage)
//  //{
//  //    Create2DDoubleMat(Rhs+1, 0, 0, &tmp);
//	 // //imageData
//  //}
//  //else if(! IplImg2Mat(pImage, Rhs+1))
//  //  {
//  //    Scierror(999, "%s: SIVP interal error.\r\n", fname);
//  //    return -1;
//  //  }
//
//  //LhsVar(1) = Rhs+1;
//
//  //pImage can not be released!!

  return 0;
}



 //  int nFile;
 //   IplImage * pImage;
	//SciErr sciErr;
	//int* piAddr     = NULL;
	//int iType       = 0;
	//int iRet        = 0;
	//int iPrec       = 0;
	//char cData  = 0;

 //   CheckInputArgument(pvApiCtx, 1, 1);
 //   CheckOutputArgument(pvApiCtx, 0, 1);
	//sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	//iRet = getScalarInteger8(pvApiCtx, piAddr, &cData);
	//nFile = cData - 1;

	//int ndims  = 3;
	//int dims_var[3] = {480,640,3};
	//int *dims = dims_var;

	//Mat dst = cvarrToMat(pImage);

	//int nBytes = dst.elemSize1();
	//int rows =480/2;
	//int cols = 640/2;
	//dst = dst.t();
	//cvSetCaptureProperty(OpenedAviCap[nFile].video.cap, CV_CAP_PROP_FRAME_WIDTH, 240);
	//cvSetCaptureProperty(OpenedAviCap[nFile].video.cap, CV_CAP_PROP_FRAME_HEIGHT, 320);
	////vSetCaptureProperty(OpenedAviCap[nFile].video.cap, CV_CAP_PROP_FRAME_WIDTH, rows,CV_CAP_PROP_FRAME_HEIGHT , cols);
	//pImage = cvQueryFrame(OpenedAviCap[nFile].video.cap);

	//Mat dst = cvarrToMat(pImage);

	//SetImage(1,dst);