/***********************************************************************
 * SIVP - Scilab Image and Video Processing toolbox
 * Original work Copyright (C) 2005  Shiqi Yu
 *
 * IPCV - Scilab Image Processing and Computer Vision toolbox
 * Original work Copyright (C) 2017  Tan Chin Luh
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***********************************************************************/

#include "Windows.h"
/*--------------------------------------------------------------------------*/
#if _WIN64
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_videostab2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_video2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_ts2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_stitching2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_photo2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_objdetect2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_nonfree2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_ml2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_legacy2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_imgproc2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_highgui2413.lib")
//#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_haartraining_engine.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_gpu2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_flann2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_features2d2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_core2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_contrib2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_calib3d2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_superres2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_gpu2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x64/lib/opencv_ocl2413.lib")
#else
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_videostab2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_video2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_ts2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_stitching2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_photo2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_objdetect2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_nonfree2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_ml2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_legacy2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_imgproc2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_highgui2413.lib")
//#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_haartraining_engine.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_gpu2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_flann2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_features2d2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_core2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_contrib2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_calib3d2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_superres2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_gpu2413.lib")
#pragma comment(lib,"../../thirdparty/opencv/windows/x86/lib/opencv_ocl2413.lib")
#endif
/*--------------------------------------------------------------------------*/
int WINAPI DllMain (HINSTANCE hInstance , DWORD reason, PVOID pvReserved)
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return 1;
}
/*--------------------------------------------------------------------------*/


