/***********************************************************************
 * IPCV - Scilab Image Processing and Computer Vision toolbox
 * Copyright (C) 2017  Tan Chin Luh
***********************************************************************/

#include "common.h"

int sci_int_imdetect_FAST(char * fname,void* pvApiCtx)
{

	CheckInputArgument(pvApiCtx, 4, 4);
	CheckOutputArgument(pvApiCtx, 0, 1);

	Mat img1;
	GetImage(1,img1,pvApiCtx);
	
	if(img1.empty())
	{
		sciprint("Can't read image\n");
		return -1;
	}

	double *val1 = NULL;
	int iRows1in = 0;
	int iCols1in = 0;
	GetDouble(2,val1,iRows1in,iCols1in,pvApiCtx);

	double *val2 = NULL;
	int iRows2in = 0;
	int iCols2in = 0;
	GetDouble(3,val2,iRows2in,iCols2in,pvApiCtx);

	double *val3 = NULL;
	int iRows3in = 0;
	int iCols3in = 0;
	GetDouble(4,val3,iRows3in,iCols3in,pvApiCtx);

	Ptr<FeatureDetector> detector;
	detector =  FeatureDetector::create("FASTX");

	//FAST(img1, keypoints1,*val);
	detector->set("threshold", *val1);
	detector->set("nonmaxSuppression", (*val2!=0));
	detector->set("type", int(*val3));
	// TYPE_5_8 = 0, TYPE_7_12 = 1, TYPE_9_16 = 2
	//detector->set("threshold", *val);

	vector<KeyPoint> keypoints1;
	detector->detect(img1, keypoints1);
	
	int iRows1 = 7;
	int iCols1 = keypoints1.size()*1;
	double* pdblReal1 = NULL;
	pdblReal1 = new double[iRows1*iCols1];

	for (int cnt = 0 ; cnt < iCols1; cnt++)
	{
		pdblReal1[iRows1*cnt] = keypoints1[cnt].pt.x;
		pdblReal1[iRows1*cnt+1] = keypoints1[cnt].pt.y;
		pdblReal1[iRows1*cnt+2] = keypoints1[cnt].size;
		pdblReal1[iRows1*cnt+3] = keypoints1[cnt].angle;
		pdblReal1[iRows1*cnt+4] = keypoints1[cnt].response;
		pdblReal1[iRows1*cnt+5] = keypoints1[cnt].octave; 
		pdblReal1[iRows1*cnt+6] = keypoints1[cnt].class_id; 
	}


	//SciErr sciErr;
	//sciErr = createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, iRows1, iCols1, pdblReal1);      
	//AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;	
	SetDouble(1,pdblReal1,iRows1,iCols1,pvApiCtx);

	////free(pdblReal);
	delete [] pdblReal1;

	return 0;
}

